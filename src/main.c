#include <fakeyaul.h>

#define FP_ONE 		1 << 16
#define FIX16_TAU 	411775

#define SCREEN_WIDTH  	320
#define SCREEN_HEIGHT 	224
#define SCREEN_SCALE 	3


fix16_vec3_t rotation = { 0, 0, 0 };

void RotateObjPair(fix16_vec3_t* pivot, fix16_vec3_t* target, fix16_t angle)
{
	fix16_t dist_sq = fix16_mul(pivot->x - target->x, pivot->x - target->x) +
		fix16_mul(pivot->z - target->z, pivot->z - target->z);
	
	fix16_t dist = fix16_sqrt(dist_sq);
	
	fix16_t rot = fix16_atan2(-(pivot->x - target->x), -(pivot->z - target->z));
	rot += angle;
	
	target->x = pivot->x + fix16_mul(fix16_cos(rot), dist);
	target->z = pivot->z + fix16_mul(fix16_sin(rot), dist);	
}


int main(int argc, char** argv)
{	
	if(argc <= 1)
	{
		printf("Please enter a mesh for display\n");
		return 0;
	}
	
	Window_Init();
	
	WINDOW mainWindow;
	
	if(!Window_Open(&mainWindow, "Sonic Ringworlds Stage 2", SCREEN_WIDTH, SCREEN_HEIGHT, SCREEN_SCALE))
	{
		Window_Cleanup();
		return -1;
	}
		
	VDP1_Init();
	VDP2_Init(&mainWindow);
	
	Matrix_init();
	
	smpc_peripheral_init();
	
	//initAudio();
	
	vdp2_scrn_back_color_set(VDP2_VRAM_ADDR(3, 0x01FFFE), RGB1555(1, 0, 3, 15));
	
	//playSound("../assets/Oneshot-01.wav", SDL_MIX_MAXVOLUME);
	
	smpc_peripheral_digital_t pad;
	

	
	int32_t meshCount = -1;
	mesh_t* meshes = mesh_LoadFromFile(argv[1], &meshCount);	
	
	if(!meshes)
	{
		return -1;
	}
		
	int32_t textureCount = -1;
	texture_t* textures = Texture_LoadFromFile(argv[1], &textureCount);
	
	printf("Number of textures: %d\n", textureCount);
	
	if(textureCount == -1)
	{
		return -1;
	}
	
	tlist_set(textures, textureCount);



	camera_t camera;
	
	camera.position.x = 0;
	camera.position.y = 0;
	camera.position.z = 0;
	
	camera.target.x = 0;
	camera.target.y = 0;
	camera.target.z = fix16_one;
	camera.up.x = 0;
	camera.up.y = fix16_one;
	camera.up.z = 0;

	fix16_vec3_t translation = { 0, 0, 0 };
	fix16_vec3_t rotation = { 0, 0, 0 };

	int32_t quit = 0;
	while(!quit)
	{
		quit = Window_PollEvents();
		
		smpc_peripheral_process();
		
		smpc_peripheral_digital_port(1, &pad);
		
		Window_ClearColour(&mainWindow, 0);



		fix16_vec3_t input = { 0, 0, 0 };
		if(pad.pressed & PERIPHERAL_DIGITAL_UP)
		{
			input.z -= FP_ONE;
		}
		if(pad.pressed & PERIPHERAL_DIGITAL_DOWN)
		{
			input.z += FP_ONE;
		}
		if(pad.pressed & PERIPHERAL_DIGITAL_LEFT)
		{	
			rotation.y -= FP_ONE >> 4;
			//if(rotation.y < 0) rotation.y += FIX16_TAU;
		}
		if(pad.pressed & PERIPHERAL_DIGITAL_RIGHT)
		{
			rotation.y += FP_ONE >> 4;
			//if(rotation.y > FIX16_TAU) rotation.y -= FIX16_TAU;
		}


		if(pad.pressed & PERIPHERAL_DIGITAL_A)
		{
			input.y -= FP_ONE >> 2;
		}
		
		if(pad.pressed & PERIPHERAL_DIGITAL_B)
		{
			mesh_OutputPolygonInfo = 1;
		}
		else
		{
			mesh_OutputPolygonInfo = 0;
		}
		
		if(pad.pressed & PERIPHERAL_DIGITAL_C)
		{
			rotation.x -= FP_ONE >> 6;
		}
		if(pad.pressed & PERIPHERAL_DIGITAL_X)
		{
			input.y += FP_ONE >> 2;
		}
		if(pad.pressed & PERIPHERAL_DIGITAL_Y)
		{

		}
		if(pad.pressed & PERIPHERAL_DIGITAL_Z)
		{
			rotation.x += FP_ONE >> 6;
		}
		
		if(pad.pressed & PERIPHERAL_DIGITAL_L)
		{
			input.x -= FP_ONE;
		}
		
		if(pad.pressed & PERIPHERAL_DIGITAL_R)
		{
			input.x += FP_ONE;
		}
		
		if(pad.pressed & PERIPHERAL_DIGITAL_START)
		{
			translation.x = 0;
			translation.y = 0;
			translation.z = 0;
			
			rotation.x = 0;
			rotation.y = 0;
			
		}

	
		if(input.x != 0 || input.z != 0)
		{
			translation.x = fix16_mul(
				fix16_cos(fix16_mul(rotation.y, 0)) - fix16_sin(fix16_mul(rotation.y, fix16_one)),
				input.z);
			translation.z = fix16_mul(
				fix16_sin(fix16_mul(rotation.y, 0)) + fix16_cos(fix16_mul(rotation.y, fix16_one)),
				input.z);
			
			/*
			 * it's STILL doing the whole "moving on one axis" thing
			translation.x += fix16_mul(
				fix16_cos(fix16_mul(rotation.y, fix16_one)) - fix16_sin(fix16_mul(rotation.y, 0)),
				input.x);
			translation.z += fix16_mul(
				fix16_sin(fix16_mul(rotation.y, fix16_one)) + fix16_cos(fix16_mul(rotation.y, 0)),
				input.x);
			*/
			
			//translation.x = fix16_cos(fix16_mul(rotation.y, input.x)) - fix16_sin(fix16_mul(rotation.y, input.z));
			//translation.z = fix16_sin(fix16_mul(rotation.y, input.x)) + fix16_cos(fix16_mul(rotation.y, input.z));

		}
		else
		{
			translation.x = 0;
			translation.z = 0;
		}
		
		translation.y = input.y;

		camera.position = fix16_vec3_add(&translation, &camera.position);
		
		
		//camera.position = fix16_vec3_add(&input, &camera.position);

		//camera.position = translation;
		camera.target = camera.position;
		
		camera.target.x += fix16_cos(fix16_mul(rotation.y, 0)) - fix16_sin(fix16_mul(rotation.y, fix16_one));
		//camera.target.y += fix16_cos(fix16_mul(rotation.x, 0)) - fix16_sin(fix16_mul(rotation.x, fix16_one));
		camera.target.z += fix16_sin(fix16_mul(rotation.y, 0)) + fix16_cos(fix16_mul(rotation.y, fix16_one));

		
		
		
		
		camera_lookat(&camera);
		
		matrix_push();
		
		/*
		matrix_x_translate(translation.x);
		matrix_y_translate(translation.y);
		matrix_z_translate(translation.z);
		*/
		
		/*
		matrix_z_rotate(rotation.z);
		matrix_y_rotate(rotation.y);
		matrix_x_rotate(rotation.x);
		*/

		
		
		printf(" CameraPosition: %d.%d %d.%d %d.%d\n", 
			(camera.position.x >> 16), (camera.position.x & 0x0000FFFF), 
			(camera.position.y >> 16), (camera.position.y & 0x0000FFFF),
			(camera.position.z >> 16), (camera.position.z & 0x0000FFFF));
		
		printf(" CameraTarget: %d.%d %d.%d %d.%d\n", 
			(camera.target.x >> 16), (camera.target.x & 0x0000FFFF), 
			(camera.target.y >> 16), (camera.target.y & 0x0000FFFF),
			(camera.target.z >> 16), (camera.target.z & 0x0000FFFF));
			
		printf(" Translation: %d.%d %d.%d %d.%d\n", 
			(translation.x >> 16), (translation.x & 0x0000FFFF), 
			(translation.y >> 16), (translation.y & 0x0000FFFF),
			(translation.z >> 16), (translation.z & 0x0000FFFF));
			
		printf(" rotation: %d.%d %d.%d %d.%d\n", 
			(rotation.x >> 16), (rotation.x & 0x0000FFFF), 
			(rotation.y >> 16), (rotation.y & 0x0000FFFF),
			(rotation.z >> 16), (rotation.z & 0x0000FFFF));
		
		
		//printf("matrix output\n");
		
		for(int32_t i = 0; i < meshCount; i++)
		{
			render_mesh_transform(&meshes[i]);
		}
		
		
		
		matrix_pop();
		
		VPD1_sync_render();
		VDP1_sync();
		
		VDP2_sync();
	}

	free(textures);

	free(meshes);
	
	//endAudio();
	
	Window_Close(&mainWindow);
	
	Window_Cleanup();
	
	return 0;
}
