# Ringworlds Model Viewer

## Usage
	Install the [SMF Exporter for Blender](https://gitlab.com/virattus/blender-smf-exporter)
	
	Export your model into a directory where you have copies of every texture used in the model+
	
	To open a file, use the command line to load the file as a parameter
	
	Example:
		RingworldModelViewer assets/bossfrost/bossfrost.smf
	
	Controls are:
	DPad Up/Down: Forward/Backward
	DPad Left/Right: Turn Left/Right
		L/R Shoulders: Strafe Left/Right
		A/X: Adjust Elevation Up/Down
		C/Z: Look Up/Down

	Keyboard equivalents are:
		WASD for DPad
		Numpad 456 for ABC
		Numpad 789 for XYZ
		Left Shift for Left Shoulder
		Numpad Plus for Right Shoulder

## Building

### Windows
	
### Linux
	mkdir build
	cd build
	cmake ..
	make

