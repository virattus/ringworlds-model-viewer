#ifndef __RINGWORLDS_FAKEYAUL_H__
#define __RINGWORLDS_FAKEYAUL_H__

#define SDL_MAIN_HANDLED

#include <core/types.h>
#include <core/window.h>
#include <core/timer.h>
#include <core/controller.h>
#include <core/scu.h>
#include <core/cd.h>
#include <core/audio.h>
#include <core/camera.h>
#include <core/object.h>
#include <core/colour.h>
#include <core/dbgio.h>

#include <gamemath/vector.h>
#include <gamemath/vectormath.h>
#include <gamemath/matrix.h>

#include <gfx/mesh.h>
#include <gfx/clip.h>
#include <gfx/displaylist.h>
#include <gfx/render.h>
#include <gfx/texturemapper.h>
#include <gfx/vram.h>
#include <gfx/primitive.h>

#include <gfx/vdp1/vdp1.h>
#include <gfx/vdp1/vdp1_env.h>
#include <gfx/vdp1/vdp1_vram.h>
#include <gfx/vdp1/cmdt.h>

#include <gfx/vdp2/vdp2.h>
#include <gfx/vdp2/vdp2_vram.h>

#include <gfx/render/software/draw.h>

#endif
