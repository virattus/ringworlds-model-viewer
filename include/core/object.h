#ifndef __RINGWORLDS_OBJECT_H__
#define __RINGWORLDS_OBJECT_H__


#include <stdint.h>


typedef struct _OBJECT
{
	uint32_t objectType;
	void* data;
	
} object_t;



void UpdateObject(object_t* obj);


#endif
