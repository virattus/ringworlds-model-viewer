#include <core/camera.h>

#include <gamemath/matrix.h>


void camera_lookat(camera_t* cam)
{
	matrix_SetViewMatrix(&cam->position, &cam->target, &cam->up);
}
