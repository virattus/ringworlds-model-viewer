#include <core/colour.h>

#include <gamemath/vectormath.h>

#include <assert.h>


#define SDL_A_MASK 0x000000FF
#define SDL_R_MASK 0xFF000000
#define SDL_G_MASK 0x00FF0000
#define SDL_B_MASK 0x0000FF00

#define RGB1555_MSB_MASK 0x8000
#define RGB1555_R_MASK		31 //0x1F
#define RGB1555_G_MASK		992 //fucking wonky
#define RGB1555_B_MASK		31744 //0x7C00


uint32_t RGB1555_to_uint32(rgb1555_t col)
{
	return (col.r << 27) + (col.g << 14) + (col.b << 1) + (0xFF); 
}


CVECTOR RGB1555_to_CVector(rgb1555_t col)
{
	CVECTOR retval;
	retval.r = (col.raw & RGB1555_R_MASK);
	retval.g = (col.raw & RGB1555_G_MASK) >> 5;
	retval.b = (col.raw & RGB1555_B_MASK) >> 10;
	retval.a = (col.raw & RGB1555_MSB_MASK) >> 15;
	
	return retval;
}



uint32_t CVector_to_uint32(CVECTOR v0)
{
	return (v0.r << 24) + (v0.g << 16) + (v0.b << 8) + 0xFF;
}


CVECTOR Uint32_to_CVector(uint32_t col)
{	
	CVECTOR retval;
	
	retval.r = (col & SDL_R_MASK) >> 24;
	retval.g = (col & SDL_G_MASK) >> 16;
	retval.b = (col & SDL_B_MASK) >> 8;
	retval.a = (col & SDL_A_MASK);
	
	return retval;
}




CVECTOR Interpolate_CVECTOR(CVECTOR v0, CVECTOR v1, fix16_t factor)
{	
	v0.r = (uint8_t)int32_interpolate(v0.r, v1.r, factor);
	v0.g = (uint8_t)int32_interpolate(v0.g, v1.g, factor);
	v0.b = (uint8_t)int32_interpolate(v0.b, v1.b, factor);
	
	return v0;
}


CVECTOR Add_CVECTOR(CVECTOR v0, CVECTOR v1)
{
	v0.r += v1.r;
	v0.g += v1.g;
	v0.b += v1.b;
	
	return v0;
}


CVECTOR Sub_CVECTOR(CVECTOR v0, CVECTOR v1)
{
	v0.r -= v1.r;
	v0.g -= v1.g;
	v0.b -= v1.b;
}


CVECTOR Scale_CVECTOR(CVECTOR v0, fix16_t multiplier)
{
	v0.r = fix16_to_int(fix16_mul(multiplier, fix16_from_int(v0.r)));
	v0.g = fix16_to_int(fix16_mul(multiplier, fix16_from_int(v0.g)));
	v0.b = fix16_to_int(fix16_mul(multiplier, fix16_from_int(v0.b)));
	
	return v0;
}


CVECTOR Mul_CVECTOR(CVECTOR v0, CVECTOR v1)
{
	v0.r = (v0.r * v1.r) / 255;
	v0.g = (v0.g * v1.g) / 255;
	v0.b = (v0.b * v1.b) / 255;
	//v0.a = (v0.a * v1.a) / 255;
	
	return v0;
}


CVECTOR Div_CVECTOR(CVECTOR v0, fix16_t divisor)
{
	assert(divisor != 0);
	
	v0.r = fix16_to_int(fix16_div(fix16_from_int(v0.r), divisor));
	v0.g = fix16_to_int(fix16_div(fix16_from_int(v0.g), divisor));
	v0.b = fix16_to_int(fix16_div(fix16_from_int(v0.b), divisor));
	
	return v0;
}
