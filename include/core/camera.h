#ifndef __RINGWORLDS_CAMERA_H__
#define __RINGWORLDS_CAMERA_H__


#include <gamemath/vector.h>


typedef struct _CAMERA
{
	fix16_vec3_t position;
	fix16_vec3_t target;
	fix16_vec3_t up;
} camera_t;


void camera_lookat(camera_t* cam);


#endif
