#ifndef __RINGWORLDS_TIMER_H__
#define __RINGWORLDS_TIMER_H__


#include <stdint.h>


uint32_t Timer_GetFPS();

int32_t Timer_UpdateFPS();


#endif
