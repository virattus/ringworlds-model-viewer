#ifndef __RINGWORLDS_TYPES_H__
#define __RINGWORLDS_TYPES_H__


#include <gamemath/vector.h>
#include <gfx/vdp1/cmdt.h>




typedef struct _MESH
{
	uint32_t polygons_count;
	fix16_vec3_t* points;
	fix16_vec3_t* normals;
	uint16_t* Colours;
	attribute_t* attributes;
	uint16_t* Indices;
	
} __aligned(4) mesh_t;



#endif
