#ifndef __RINGWORLDS_MATRIX_H__
#define __RINGWORLDS_MATRIX_H__



#include <gamemath/vector.h>

#include <stdint.h>


#define FIX16_MAT43_COLUMNS 	(4)
#define FIX16_MAT43_ROWS		(3)
#define FIX16_MAT43_ARR_COUNT	(FIX16_MAT43_COLUMNS * FIX16_MAT43_ROWS)


//ROW MAJOR MATRIX
typedef union fix16_mat43
{
	struct
	{
		fix16_t m00, m01, m02, m03;
		fix16_t m10, m11, m12, m13;
		fix16_t m20, m21, m22, m23;
	} comp;
	
	fix16_t arr[FIX16_MAT43_ARR_COUNT];
	
	fix16_t frow[FIX16_MAT43_ROWS][FIX16_MAT43_COLUMNS];
	
	fix16_t row[FIX16_MAT43_ROWS];
	
} fix16_mat43_t;


typedef struct mstack
{
	fix16_mat43_t* pool_matrix;
	fix16_mat43_t* top_matrix;
	fix16_mat43_t* bottom_matrix;
	uint32_t stack_count;
	
} mstack_t;


void __matrix_init(void);
fix16_mat43_t* __matrix_view_get(void);


#ifdef __cplusplus
extern "C"
{
#endif

void Matrix_init();

void matrix_push();
void matrix_pop();

void matrix_x_translate(int32_t x);
void matrix_y_translate(int32_t y);
void matrix_z_translate(int32_t z);

void matrix_x_rotate(int32_t theta);
void matrix_y_rotate(int32_t theta);
void matrix_z_rotate(int32_t theta);

void matrix_eval(fix16_vec3_t* pos);

void matrix_SetViewMatrix(const fix16_vec3_t* pos, const fix16_vec3_t* target, const fix16_vec3_t* up);


#ifdef __cplusplus
}
#endif

#endif
