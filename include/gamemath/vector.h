#ifndef __RINGWORLDS_VECTOR_H__
#define __RINGWORLDS_VECTOR_H__


#include <libfixmath/fix16.h>


#define min(a,b) \
	({ __typeof__ (a) _a = (a); \
		__typeof__ (b) _b = (b); \
		_a < _b ? _a : _b; })

#define max(a,b) \
	({ __typeof__ (a) _a = (a); \
		__typeof__ (b) _b = (b); \
		_a > _b ? _a : _b; })




typedef struct _VECTOR2
{
	int32_t x, y;
} fix16_vec2_t, int32_vec2_t;


typedef struct _VECTOR3
{
	int32_t x, y, z;
} fix16_vec3_t, int32_vec3_t;


typedef struct _VECTOR4
{
	int32_t x, y, z, w;
} fix16_vec4_t, int32_vec4_t;


typedef struct _SVECTOR2
{
	int16_t x, y;
} int16_vec2_t;


typedef struct _SVECTOR3
{
	int16_t x, y, z;
} int16_vec3_t;



#endif
