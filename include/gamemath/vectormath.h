#ifndef __RINGWORLDS_VECTORMATH_H__
#define __RINGWORLDS_VECTORMATH_H__



#include <gamemath/vector.h>


fix16_vec3_t fix16_vec3_add(const fix16_vec3_t* v0, const fix16_vec3_t* v1);
fix16_vec3_t fix16_vec3_sub(const fix16_vec3_t* v0, const fix16_vec3_t* v1);

fix16_t fix16_vec3_dot(const fix16_vec3_t* v0, const fix16_vec3_t* v1);
fix16_t fix16_vec2_dot(const fix16_vec2_t* v0, const fix16_vec2_t* v1);

fix16_t fix16_vec2_length(const fix16_vec2_t* v);
fix16_t fix16_vec3_length(const fix16_vec3_t* v);


void fix16_vec3_normalize(fix16_vec3_t* v);


int32_t int32_interpolate(int32_t i0, int32_t i1, fix16_t factor);

int32_vec2_t int32_vec2_interpolate(const int32_vec2_t* v0, const int32_vec2_t* v1, fix16_t factor);
int32_vec3_t int32_vec3_interpolate(const int32_vec3_t* v0, const int32_vec3_t* v1, fix16_t factor);




#endif
