//OH NO ITS C++

#include <gamemath/matrix.h>


#include <vector>
#include <glm/glm.hpp>
#include <glm/ext.hpp>

#include <stdio.h>
#include <math.h>
#include <assert.h>

#define FLOAT_FIX16_ONE 65536.0f
#define Z_NEAR 0.1f


#define min(a,b) \
	({ __typeof__ (a) _a = (a); \
		__typeof__ (b) _b = (b); \
		_a < _b ? _a : _b; })

#define max(a,b) \
	({ __typeof__ (a) _a = (a); \
		__typeof__ (b) _b = (b); \
		_a > _b ? _a : _b; })


glm::mat4 Proj;
glm::mat4 View;

glm::mat4 Model;

std::vector<glm::mat4> matrixStack;


glm::mat4 GeneratePerspective(float aspect, float fov, float zNear, float zFar)
{
	glm::mat4 output(0);
	
	float top = zNear * tanf(fov * M_PI * M_PI / 64800.0f);
	float bottom = -top;
	float left = bottom * aspect;
	float right = top * aspect;
		
	printf("top: %f bottom: %f left %f right %f\n", top, bottom, left, right);
	
	output[0][0] = (2 * zNear) / (right - left);
	output[1][1] = (2 * zNear) / (top - bottom);
	output[3][2] = -(2 * zFar * zNear) / (zFar - zNear);
	
	output[2][0] = (right + left) / (right - left);
	output[2][1] = (top + bottom) / (top - bottom);
	output[2][2] = -(zFar + zNear) / (zFar - zNear);
	
	output[2][3] = -1;
	
	return output;
	
}



void Matrix_init()
{
	//Proj = glm::mat4(1);
	//Proj = glm::ortho(0.0f, 320.0f, 0.0f, 224.0f, 0.1f, 100.0f);
	Proj = glm::perspective(glm::radians(45.0f), 1.33333f, Z_NEAR, 10.0f);
	View = glm::mat4(1);
	//View = glm::lookAt(glm::vec3(3, 8, -24), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	
	Model = glm::mat4(1);
	
	/*
	glm::mat4 output = glm::perspective(glm::radians(45.0f), 1.0f, 0.1f, 1000.0f);
	printf("perspective output:\n");
	printf("%f %f %f %f\n", output[0][0], output[0][1], output[0][2], output[0][3]);
	printf("%f %f %f %f\n", output[1][0], output[1][1], output[1][2], output[1][3]);
	printf("%f %f %f %f\n", output[2][0], output[2][1], output[2][2], output[2][3]);
	printf("%f %f %f %f\n", output[3][0], output[3][1], output[3][2], output[3][3]);
	
	output = GeneratePerspective(45.0f, 1.0f, 0.1f, 1000.0f);
	printf("my own perspective output:\n");
	printf("%f %f %f %f\n", output[0][0], output[0][1], output[0][2], output[0][3]);
	printf("%f %f %f %f\n", output[1][0], output[1][1], output[1][2], output[1][3]);
	printf("%f %f %f %f\n", output[2][0], output[2][1], output[2][2], output[2][3]);
	printf("%f %f %f %f\n", output[3][0], output[3][1], output[3][2], output[3][3]);
	*/
	
	matrixStack.push_back(Model);
}


void matrix_push()
{	
	glm::mat4 newMat(1.0);
	
	matrixStack.push_back(newMat);
}


void matrix_pop()
{	
	matrixStack.pop_back();
}


void matrix_x_translate(int32_t x)
{
	assert(matrixStack.size() > 0);
	uint32_t stackBack = matrixStack.size() - 1;
	
	matrixStack[stackBack] = glm::translate(matrixStack[stackBack], glm::vec3(x / FLOAT_FIX16_ONE, 0, 0));
	//Model = glm::translate(Model, glm::vec3(x / FLOAT_FIX16_ONE, 0, 0));
}


void matrix_y_translate(int32_t y)
{
	assert(matrixStack.size() > 0);
	uint32_t stackBack = matrixStack.size() - 1;
	
	matrixStack[stackBack] = glm::translate(matrixStack[stackBack], glm::vec3(0, y / FLOAT_FIX16_ONE, 0));
	//Model = glm::translate(Model, glm::vec3(0, y / FLOAT_FIX16_ONE, 0));
}


void matrix_z_translate(int32_t z)
{
	assert(matrixStack.size() > 0);
	uint32_t stackBack = matrixStack.size() - 1;
	
	matrixStack[stackBack] = glm::translate(matrixStack[stackBack], glm::vec3(0, 0, z / FLOAT_FIX16_ONE));
	//Model = glm::translate(Model, glm::vec3(0, 0, z / FLOAT_FIX16_ONE));
}



void matrix_x_rotate(int32_t theta)
{
	assert(matrixStack.size() > 0);
	uint32_t stackBack = matrixStack.size() - 1;
	
	matrixStack[stackBack] = glm::rotate(matrixStack[stackBack], glm::radians(theta / FLOAT_FIX16_ONE), glm::vec3(1, 0, 0));
	//Model = glm::rotate(Model, glm::radians(theta / FLOAT_FIX16_ONE), glm::vec3(1, 0, 0));
}


void matrix_y_rotate(int32_t theta)
{
	assert(matrixStack.size() > 0);
	uint32_t stackBack = matrixStack.size() - 1;
	
	matrixStack[stackBack] = glm::rotate(matrixStack[stackBack], glm::radians(theta / FLOAT_FIX16_ONE), glm::vec3(0, 1, 0));
	//Model = glm::rotate(Model, glm::radians(theta / FLOAT_FIX16_ONE), glm::vec3(0, 1, 0));
}


void matrix_z_rotate(int32_t theta)
{
	assert(matrixStack.size() > 0);
	uint32_t stackBack = matrixStack.size() - 1;
	
	matrixStack[stackBack] = glm::rotate(matrixStack[stackBack], glm::radians(theta / FLOAT_FIX16_ONE), glm::vec3(0, 0, 1));
	//Model = glm::rotate(Model, glm::radians(theta / FLOAT_FIX16_ONE), glm::vec3(0, 0, 1));
}


void matrix_eval(fix16_vec3_t* pos)
{
	glm::mat4 matrix = Proj * View;
	
	
	for(uint32_t i = 0; i < matrixStack.size(); i++)
	{
		matrix *= matrixStack[i];
	}
	
	
	glm::vec4 newPos(pos->x / 65536.0, pos->y / 65536.0, pos->z / 65536.0, 1.0f);

	
	newPos = matrix * newPos;

	newPos.z = max(newPos.z, Z_NEAR);	
	
	//newPos.w /= 65536.0f;
	
	if(newPos.w != 0.0)
	{
		newPos /= newPos.w;
	}
	
	//printf("result of matrix math %f %f %f %f\n", newPos.x, newPos.y, newPos.z, newPos.w);
	
	newPos = ((newPos + 1.0f) / 2.0f);
	newPos.x = 
	
	pos->x = (int32_t)roundf(newPos.x * 320.0f);
	pos->y = (int32_t)roundf(newPos.y * 224.0f);
	pos->z = (int32_t)(newPos.z * FLOAT_FIX16_ONE);
}


/*
void matrix_build(MATRIX* m)
{
	glm::mat4 matrix = Proj * View;
		
	for(uint32_t i = 0; i < matrixStack.size(); i++)
	{
		matrix *= matrixStack[i];
	}
	
	m->rotX = { (int32_t)matrix[0][0], (int32_t)matrix[0][1], (int32_t)matrix[0][2] };
	m->rotY = { (int32_t)matrix[1][0], (int32_t)matrix[1][1], (int32_t)matrix[1][2] };
	m->rotZ = { (int32_t)matrix[2][0], (int32_t)matrix[2][1], (int32_t)matrix[2][2] };
	m->origin = { (int32_t)matrix[3][0], (int32_t)matrix[3][1], (int32_t)matrix[3][2] };
	
}
*/


void matrix_SetViewMatrix(const fix16_vec3_t* pos, const fix16_vec3_t* target, const fix16_vec3_t* up)
{
	View = glm::lookAt(glm::vec3(pos->x / FLOAT_FIX16_ONE, pos->y / FLOAT_FIX16_ONE, pos->z / FLOAT_FIX16_ONE), 
		glm::vec3(target->x / FLOAT_FIX16_ONE, target->y / FLOAT_FIX16_ONE, target->z / FLOAT_FIX16_ONE), 
		glm::vec3(up->x / FLOAT_FIX16_ONE, up->y / FLOAT_FIX16_ONE, up->z / FLOAT_FIX16_ONE));
}
