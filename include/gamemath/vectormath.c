#include <gamemath/vectormath.h>


#define FIXMATH_NO_64BIT
#include <libfixmath/fix16.h>

#include <math.h>
#include <assert.h>


fix16_vec3_t fix16_vec3_add(const fix16_vec3_t* v0, const fix16_vec3_t* v1)
{
	assert(v0);
	assert(v1);
	
	fix16_vec3_t v = {
		v0->x + v1->x,
		v0->y + v1->y,
		v0->z + v1->z
	};
	
	return v;
}


fix16_vec3_t fix16_vec3_sub(const fix16_vec3_t* v0, const fix16_vec3_t* v1)
{
	assert(v0);
	assert(v1);
	
	fix16_vec3_t v = { 
		fix16_sub(v0->x, v1->x),
		fix16_sub(v0->y, v1->y),
		fix16_sub(v0->z, v1->z),		
	};
	
	return v;
}


fix16_t fix16_vec3_dot(const fix16_vec3_t* v0, const fix16_vec3_t* v1)
{
	assert(v0);
	assert(v1);
	
	return fix16_add(fix16_add(fix16_mul(v0->x, v1->x), fix16_mul(v0->y, v1->y)), fix16_mul(v0->z, v1->z));
}


fix16_t fix16_vec2_dot(const fix16_vec2_t* v0, const fix16_vec2_t* v1)
{
	assert(v0);
	assert(v1);
	
	return fix16_add(fix16_mul(v0->x, v1->x), fix16_mul(v0->y, v1->y));
}


fix16_t fix16_vec2_length(const fix16_vec2_t* v)
{
	assert(v);
	
	return fix16_sqrt( fix16_mul(v->x, v->x) +
						fix16_mul(v->y, v->y));
}


fix16_t fix16_vec3_length(const fix16_vec3_t* v)
{
	assert(v);
	
	return fix16_sqrt((v->x * v->x) +
				(v->y * v->y) +
				(v->z * v->z));
}


void fix16_vec3_normalize(fix16_vec3_t* v)
{
	fix16_t magnitude = fix16_vec3_length(v);
	
	v->x = fix16_div(v->x, magnitude);
	v->y = fix16_div(v->y, magnitude);
	v->z = fix16_div(v->z, magnitude);
}


int32_t int32_interpolate(int32_t i0, int32_t i1, fix16_t factor)
{
	return fix16_to_int(fix16_mul(fix16_from_int(i0), (fix16_one - factor)) + fix16_mul(fix16_from_int(i1), factor));
}


int32_vec2_t int32_vec2_interpolate(const int32_vec2_t* v0, const int32_vec2_t* v1, fix16_t factor)
{
	int32_vec2_t v = {
		int32_interpolate(v0->x, v1->x, factor),
		int32_interpolate(v0->y, v1->y, factor),
	};

	return v;
}


int32_vec3_t int32_vec3_interpolate(const int32_vec3_t* v0, const int32_vec3_t* v1, fix16_t factor)
{
	int32_vec3_t v = {
		int32_interpolate(v0->x, v1->x, factor),
		int32_interpolate(v0->y, v1->y, factor),
		int32_interpolate(v0->z, v1->z, factor),
	};
	
	return v;
}
