#ifndef __RINGWORLDS_ORDERTABLE_H__
#define __RINGWORLDS_ORDERTABLE_H__


#include <gfx/primitive.h>


#define POLYGON_MAX 4000


void DisplayList_Clear();

void DisplayList_AddPolygon(POLYGON poly);

void DisplayList_Sort();

POLYGON* DisplayList_GetPolygon(uint32_t offset);

void DisplayList_WriteToFile();

#endif
