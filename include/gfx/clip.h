#ifndef __RINGWORLDS_CLIP_H__
#define __RINGWORLDS_CLIP_H__


#include <gfx/primitive.h>
#include <stdint.h>


int32_t QuadVisible(const POLYGON* poly, const RECT* rect);


#endif
