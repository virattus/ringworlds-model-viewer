#ifndef __RINGWORLDS_TEXTURE_MAPPER_H__
#define __RINGWORLDS_TEXTURE_MAPPER_H__


#include <gamemath/vector.h>

#include <stdint.h>

/*
 * Texture sizes are as such:
 * 	x must be a multiple of eight, with a max of 31 equaling a max size of 248
 * 	y is a simple range from 1 to 255
 * 
 * x is stored by bit shifting right 3 then shifting left 8
 */



typedef struct texture
{
	uint32_t vram_offset;
	int16_vec2_t size;
} texture_t;


typedef struct picture
{
	const void* data;
	uint16_t data_size;
	int16_vec2_t dim;
	uint16_t palette_index;
} picture_t;


typedef struct texturelist
{
	texture_t* textures;
	uint16_t count;
} tlist_t;


texture_t* Texture_LoadFromFile(const char* texListFile, int32_t* texCount);

void tlist_set(texture_t* textures, uint16_t count);


void TextureMapper_Init();





#endif
