#ifndef __RINGWORLDS_RENDER_H__
#define __RINGWORLDS_RENDER_H__


#include <core/types.h>
#include <gfx/texturemapper.h>


typedef struct _RENDER_TRANSFORM
{
	int16_vec2_t screen_points[4];
	fix16_t z_values[4];
} render_transform_t;


typedef struct _RENDER
{
	
	
	render_transform_t* render_transform;
} render_t;


typedef struct _ENGINESTATE
{
	render_t* render;
	tlist_t tlist;
} state_t;


extern state_t __state;

void __render_init(void);


#endif
