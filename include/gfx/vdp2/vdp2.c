#include <gfx/vdp2/vdp2.h>

#include <gfx/vram.h>
#include <gfx/vdp1/vdp1.h>
#include <core/window.h>
#include <core/timer.h>

#define TEXTURE_SIZE 512

WINDOW* mainWindow;
WINDOW vdp2vramOutput;

VRAM BackgroundRAM;
uint32_t BackgroundColour = 0;


void VDP2_Init(WINDOW* main)
{
	mainWindow = main;
	
	int32_vec2_t RAMSize = { TEXTURE_SIZE, TEXTURE_SIZE };
	VRAM_Init(&BackgroundRAM, &RAMSize, 0xFFFF00FF);

#ifdef SHOW_VRAM
	Window_Open(&vdp2vramOutput, "VDP2 VRAM Display", TEXTURE_SIZE, TEXTURE_SIZE, 1);
#endif
}


void VDP2_Delete()
{
#ifdef SHOW_VRAM
	Window_Close(&vdp2vramOutput);
#endif
	
	SDL_FreeSurface(BackgroundRAM.RAMSurface);
}


void vdp2_scrn_back_color_set(vdp2_vram_t vram_ptr, rgb1555_t colour)
{
	BackgroundColour = RGB1555_to_uint32(colour);
}


uint32_t GeneratePixelColour()
{
} 


void WriteToWindow()
{
	int WindowTotalSize = 0;
	for(int i = 0; i < WindowTotalSize; i++)
	{
		GeneratePixelColour();
	}
}


void VDP2_sync()
{
	Window_ClearColour(mainWindow, BackgroundColour);
	
	Window_BlitBuffer(mainWindow, VDP1_GetDisplayFramebuffer()->RAMSurface);

	if(Timer_UpdateFPS())
	{
		char newTitle[64];
		sprintf(newTitle, "Sonic Ringworlds Stage 2 (FPS: %d)", Timer_GetFPS());
		Window_SetTitle(mainWindow, newTitle);
	}

#ifdef SHOW_VRAM
	Window_BlitBuffer(&vdp2vramOutput, BackgroundRAM.RAMSurface);
#endif
}

void VDP2_sync_wait()
{
}
