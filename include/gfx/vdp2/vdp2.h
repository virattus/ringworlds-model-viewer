#ifndef __RINGWORLDS_VDP2_H__
#define __RINGWORLDS_VDP2_H__


#include <core/window.h>
#include <core/colour.h>
#include <gfx/vram.h>
#include <gfx/vdp2/vdp2_vram.h>

#include <stdint.h>


#define VDP2_VRAM_ADDR(bank, offset) (((bank) << 17) + (offset))


void VDP2_Init(WINDOW* mainWindow);
void VDP2_Delete();


void vdp2_scrn_back_color_set(vdp2_vram_t vram_ptr, rgb1555_t colour);


void VDP2_sync();

void VDP2_sync_wait();


#endif
