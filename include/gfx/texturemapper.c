#include <gfx/texturemapper.h>

#include <gamemath/vector.h>
#include <gfx/render.h>
#include <gfx/vram.h>
#include <gfx/vdp1/vdp1.h>

#include <stdint.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>


#define BUFFER_SIZE 1024
int32_t LinesInFile(const char* filename)
{
	FILE* file = fopen(filename, "r");
	if(!file)
	{
		printf("failed to open file: %s\n", filename);
		return -1;
	}
	
	char* buffer = (char*)malloc(BUFFER_SIZE);
	
	int counter = 0;
	
	while(fscanf(file, "%s", buffer) != EOF)
	{
		if(strlen(buffer) > 3)
		{
			counter++;
		}
	}
	
	fclose(file);
	
	free(buffer);

	return counter;
}


char* GetRootDirectory(const char* filename)
{
	int nameLength = strlen(filename);
	char* Root = (char*)malloc(nameLength);

	int delim = 0;	
	for(int i = 0; i < nameLength; i++)
	{
		if(filename[i] == '/')
		{
			delim = i;
		}
	}

	//printf("string of length %d - delim found at %d\n", nameLength, delim);

	memcpy(Root, filename, delim);
	Root[delim] = '\0';
	
	return Root;
}


texture_t* Texture_LoadFromFile(const char* texListFile, int32_t* texCount)
{	
	printf("loading textures related to %s\n", texListFile);
	
	size_t filenameLen = strlen(texListFile) + 10;
	char* TextureFile = (char*)malloc(filenameLen);
	sprintf(TextureFile, "%s.textures", texListFile);
	
	int32_t lineCount = LinesInFile(TextureFile);
	
	texture_t* texArray = (texture_t*)malloc(sizeof(texture_t) * lineCount);
	
	
	FILE* handler = fopen(TextureFile, "r");
	if(!handler)
	{
		printf("Failed to open texture descriptor file: %s\n", texListFile);
		free(texArray);
		return NULL;
	}
	
	char* TextureRoot = GetRootDirectory(texListFile);
	printf("texture root directory: %s\n", TextureRoot);
	
	uint32_t offset = 0;
	uint32_t texID = 0;
	int16_vec2_t dimensions = { 0, 0 };
	
	char* buffer = (char*)malloc(256);
	int string_size;
	
	if(handler)
	{
		char* texPath = (char*)malloc(256);
		while(fscanf(handler, "%s", buffer) != EOF)
		{
			sprintf(texPath, "%s/%s", TextureRoot, buffer);
			printf("%s\n", texPath);
			
			VRAM_WriteTexture(VDP1_GetTexture(), offset, &dimensions, texPath);
		
			texArray[texID].vram_offset = offset;
			texArray[texID].size = dimensions;
		
			offset += dimensions.x * dimensions.y;
			
			texID++;
		}
		free(texPath);
	}
	
	fclose(handler);
	
	free(TextureRoot);
	free(buffer);
	
	*texCount = lineCount;
	return texArray;
}


void tlist_set(texture_t* textures, uint16_t count)
{	
	__state.tlist.textures = textures;
	__state.tlist.count = count;
}



void TextureMapper_Init()
{
}
