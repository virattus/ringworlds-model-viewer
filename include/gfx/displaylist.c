#include <gfx/displaylist.h>


#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>



POLYGON PolygonList[POLYGON_MAX];
uint32_t PolygonCount = 0;


int PolySort(const void* p0, const void* p1)
{
	if(p0 == NULL || p1 == NULL)
	{
		return 0;
	}
	
	int32_t z0 = ((POLYGON*)p0)->zSort;
	int32_t z1 = ((POLYGON*)p1)->zSort;
	
	if(z0 > z1) return 1;
	if(z0 < z1) return -1;
	
	return 0;
}


void DisplayList_Clear()
{
	memset(&PolygonList[0], 0, POLYGON_MAX * sizeof(POLYGON));
	PolygonCount = 0;
}


void DisplayList_AddPolygon(POLYGON poly)
{	
	if(PolygonCount < POLYGON_MAX)
	{
		PolygonList[PolygonCount] = poly;
		PolygonCount += 1;
		return;
	}
	
	printf("Error: exceeding max polygon count\n");
	return;
}


void DisplayList_Sort()
{
	if(PolygonCount > 0)
	{
		qsort(PolygonList, PolygonCount, sizeof(POLYGON), PolySort);
	}
	
	//printf("Sorted %d polygons\n", PolygonCount);
}


POLYGON* DisplayList_GetPolygon(uint32_t offset)
{
	if(PolygonCount == 0)
	{
		return NULL;
	}
	
	if(offset > PolygonCount - 1 || offset > POLYGON_MAX - 1)
	{
		return NULL;
	}
	
	if(offset > PolygonCount)
	{
		printf("reading past end of array: %d : %d\n", offset, PolygonCount);
		return NULL;
	}
	
	return &PolygonList[offset];
}


void DisplayList_WriteToFile()
{

}
