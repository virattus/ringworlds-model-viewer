#include <gfx/vdp1/vdp1.h>

#include <gfx/vdp1/vdp1_env.h>
#include <gfx/vram.h>
#include <core/window.h>
#include <gfx/displaylist.h>
#include <gfx/render/software/draw.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <stdio.h>
#include <assert.h>

#define FRAMEBUFFER_SIZE_X 	320
#define FRAMEBUFFER_SIZE_Y 	224
#define TEXTURE_AREA_SIZE 	512

#define BOUNDARY			1024


VRAM Framebuffers[2];
uint32_t ActiveFramebuffer = 0;

VRAM TextureArea;


WINDOW vramOutput;


void VDP1_Init()
{	
	vdp1_env_default_set();
	
	int32_vec2_t FramebufferSize = { FRAMEBUFFER_SIZE_X, FRAMEBUFFER_SIZE_Y };
	
	VRAM_Init(&Framebuffers[0], &FramebufferSize, 0);
	VRAM_Init(&Framebuffers[1], &FramebufferSize, 0);
	
	int32_vec2_t TextureSize = { TEXTURE_AREA_SIZE, TEXTURE_AREA_SIZE };
	VRAM_Init(&TextureArea, &TextureSize, 0xFFFF00FF);
	
#ifdef SHOW_VRAM
	Window_Open(&vramOutput, "VDP1 VRAM Display", TEXTURE_AREA_SIZE, TEXTURE_AREA_SIZE, 1);
#endif
}


void VDP1_Delete()
{
#ifdef SHOW_VRAM
	Window_Close(&vramOutput);
#endif
	
	SDL_FreeSurface(Framebuffers[0].RAMSurface);
	SDL_FreeSurface(Framebuffers[1].RAMSurface);
	SDL_FreeSurface(TextureArea.RAMSurface);
}


VRAM* VDP1_GetFramebuffer(uint32_t ID)
{
	assert(ID < 2);
	
	return &Framebuffers[ID];
}


VRAM* VDP1_GetDisplayFramebuffer()
{
	return VDP1_GetFramebuffer(ActiveFramebuffer ^ 1);
}


VRAM* VDP1_GetDrawFramebuffer()
{
	return VDP1_GetFramebuffer(ActiveFramebuffer);
}


VRAM* VDP1_GetTexture()
{
	return &TextureArea;
}


int32_t VDP1_VertexInBounds(VERTEX* v)
{
	if((v->pos.x < -BOUNDARY || v->pos.x > BOUNDARY) ||
		(v->pos.y < -BOUNDARY || v->pos.y > BOUNDARY))
	{
		return 0;
	}
	
	return 1;
}


void VDP1_SortHorizontal(VERTEX* v0, VERTEX* v1)
{
	if(v0->pos.x > v1->pos.x)
	{
		VERTEX temp = *v1;
		*v1 = *v0;
		*v0 = temp;
	}
}

void VDP1_SortVertical(VERTEX* v0, VERTEX* v1)
{
	if(v0->pos.y < v1->pos.y)
	{
		VERTEX temp = *v1;
		*v1 = *v0;
		*v0 = temp;
	}
}


void VDP1_DrawPolygon(POLYGON* p)
{	
	POLYGON l = *p;
	
	DrawState_t state;
	state.frameBuffer = VDP1_GetDrawFramebuffer();
	state.textureRam = VDP1_GetTexture();
	
	state.use_texture = 1;
	state.texture = l.flags.textureID;
	
	state.draw_mesh = l.flags.draw_mesh;
	state.draw_transparent = 0;
	
	/*
	l.v[1].pos.x += 1;
	l.v[2].pos.x += 1;
	l.v[2].pos.y += 1;
	l.v[3].pos.y += 1;
	*/	
	
	//lmao this was busting the rendering algo, welp
	//VDP1_SortHorizontal(&l.v[0], &l.v[1]);
	//VDP1_SortHorizontal(&l.v[3], &l.v[2]); //remember that we're going clockwise
	
	//VDP1_SortVertical(&l.v[0], &l.v[3]);
	//VDP1_SortVertical(&l.v[1], &l.v[2]);
	
	//Make sure that we skip polys with absurd sizes
	for(int i = 0; i < 4; i++)
	{
		if(!VDP1_VertexInBounds(&l.v[i]))
		{
			return;
		}
	}
	
	//printf("Texture ID: %d\n", ActiveTexture);
	
	DrawPolygon(&state, &l.v[0], &l.v[1], &l.v[2], &l.v[3]);
}



void VPD1_sync_render()
{
	DisplayList_Sort();
	
	uint32_t pCount = 0;
	POLYGON* p = DisplayList_GetPolygon(pCount);	
	while(p)
	{
		VDP1_DrawPolygon(p);
		p = DisplayList_GetPolygon(++pCount);
	}
	
	//printf("Rendered %d polys\n", pCount);
	
	DisplayList_Clear();
}


void VDP1_sync()
{
	//Window_BlitBuffer(mainWindow, Framebuffers[ActiveFramebuffer].RAMSurface);
	ActiveFramebuffer ^= 1;
	VRAM_Fill(&Framebuffers[ActiveFramebuffer], 0x00000000);

#ifdef SHOW_VRAM
	Window_BlitBuffer(&vramOutput, TextureArea.RAMSurface);
#endif
}


void VDP1_sync_wait()
{
	
}
