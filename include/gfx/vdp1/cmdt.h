#ifndef __RINGWORLDS_CMD_T_H__
#define __RINGWORLDS_CMD_T_H__


#include <core/colour.h>

#include <stdint.h>
#include <stdbool.h>


#define __packed		__attribute__ ((__packed__))
#define __aligned(x)	__attribute__ ((__aligned__ (x)))


typedef uint16_t texture_slot_t;
typedef uint16_t gst_slot_t;


typedef enum _sort_type
{
	SORT_TYPE_BFR,
	SORT_TYPE_MIN,
	SORT_TYPE_MAX,
	SORT_TYPE_CENTER,
} sort_type_t;


typedef enum _plane_type
{
	PLANE_TYPE_SINGLE,
	PLANE_TYPE_DOUBLE,
} plane_type_t;


typedef enum read_dir //This handles the orientation of the texture on the face of the polygon
{
	READ_DIR_NORMAL = 0,
	READ_DIR_H = 1,
	READ_DIR_V = 2,
	READ_DIR_HV = 3,
	READ_DIR_MAX = 4,
} read_dir_t;


typedef enum _command_type
{
	COMMAND_TYPE_SPRITE 			= 0,
	COMMAND_TYPE_SCALED_SPRITE 		= 1,
	COMMAND_TYPE_DISTORTED_SPRITE 	= 2,
	COMMAND_TYPE_POLYGON 			= 4,
	COMMAND_TYPE_POLYLINE 			= 5,
	COMMAND_TYPE_LINE 				= 6,
} command_type_t;


typedef union vdp1_cmdt_draw_mode
{
	struct
	{
		unsigned int msb_enable :1;
		unsigned int unknown0 :2;
		unsigned int hss_enable :1;
		unsigned int pre_clipping_disable :1;
		unsigned int mesh_enable :1;
		unsigned int end_code_disable :1;
		unsigned int trans_pixel_disable :1;
		unsigned int color_mode :3; //TODO replace with struct
		unsigned int cc_mode :3; //TODO replace with struct
		unsigned int missedOne :2; 
	} __packed;
	uint16_t raw;
	
} __packed vdp1_cmdt_draw_mode_t;


typedef enum _link_type
{
	LINK_TYPE_JUMP_NEXT = 0,
} link_type_t;


typedef union _indices 
{
	struct {
		uint16_t p0;
		uint16_t p1;
		uint16_t p2;
		uint16_t p3;
	};
	
	uint16_t p[4];
} __aligned(4) indices_t;


typedef union _flags
{
	unsigned int unknown0 :11;
	sort_type_t sort_type :3;
	plane_type_t plane_type :1;
	bool use_texture;
}__packed __aligned(2) flags_t;


typedef struct _polygon
{
	flags_t flags;
	indices_t indices;
} __aligned(4) polygon_t;


/*
typedef struct _attribute
{
	union
	{
		struct
		{
			//unsigned int unknown0 :1;
			//link_type_t link_type :3;
			//unsigned int unknown1 :6;
			//read_dir_t read_dir :2;
			//command_type_t command:4;
			read_dir_t read_dir_full; //temp
		} __packed;
		
		uint16_t raw;
	} __packed __aligned(2) control;
	
	vdp1_cmdt_draw_mode_t draw_mode;
	
	union
	{
		uint16_t vram_index;
		uint16_t color_bank; //TODO replace with struct
		uint16_t raw;
		rgb1555_t base_color;
	} palette_data;
	
	texture_slot_t texture_slot;
	gst_slot_t shading_slot;
	
} __aligned(4) attribute_t;
*/

typedef struct _attribute
{
	uint16_t control;
	
	texture_slot_t texture_slot;
	
	unsigned int Padding[2];
} attribute_t;






#endif
