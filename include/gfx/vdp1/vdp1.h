#ifndef __RINGWORLDS_VDP1_H__
#define __RINGWORLDS_VDP1_H__



#include <gamemath/vector.h>
#include <gfx/render/software/draw.h>
#include <gfx/vram.h>
#include <gfx/vdp1/vdp1_env.h>

#include <stdint.h>


void VDP1_Init();
void VDP1_Delete();

void vdp1_sync_interval_set(int8_t interval);


VRAM* VDP1_GetFramebuffer(uint32_t ID);
VRAM* VDP1_GetDisplayFramebuffer();
VRAM* VDP1_GetDrawFramebuffer();
VRAM* VDP1_GetTexture();


int32_t VDP1_VertexInBounds(VERTEX* v);

void VDP1_SortHorizontal(VERTEX* v0, VERTEX* v1);
void VDP1_SortVertical(VERTEX* v0, VERTEX* v1);
void VDP1_DrawPolygon(POLYGON* p);


void VPD1_sync_render();
void VDP1_sync();
void VDP1_sync_wait();

#endif
