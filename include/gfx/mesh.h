#ifndef __RINGWORLDS_MESH_H__
#define __RINGWORLDS_MESH_H__


#include <core/types.h>
#include <gamemath/vector.h>
#include <gfx/primitive.h>

extern int32_t mesh_OutputPolygonInfo;

void render_mesh_transform(mesh_t* mesh);


mesh_t* mesh_LoadFromFile(const char* filename, int32_t* count);


#endif
