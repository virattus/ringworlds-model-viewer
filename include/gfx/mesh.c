#include <gfx/mesh.h>

#include <gamemath/matrix.h>
#include <gfx/displaylist.h>
#include <gfx/clip.h>
#include <gfx/render.h>

#include <libfixmath/fix16.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>


#define INDICES_PER_POLY 4
#define HEADER_OFFSET 8
#define HEADER_BYTE_SIZE 24

#define MESH_MAGIC_V2 0xDEADBEEF
#define MESH_MAGIC_V3 0xABBAABBA
#define MESH_MAGIC_V4 0x12345678

#define MESH_MAGIC_CURRENT MESH_MAGIC_V4



int32_t mesh_OutputPolygonInfo = 0;


RECT clipRect = {
	.min = { 0, 0 },
	.max = { 319, 223 },
};


void UV_Swap2(int32_vec2_t* t0, int32_vec2_t* t1)
{
	int32_vec2_t temp = *t0;
	*t0 = *t1;
	*t1 = temp;
}


void UV_Swap(int32_vec2_t* t0, int32_vec2_t* t1, int32_vec2_t* t2, int32_vec2_t* t3, read_dir_t dir)
{
	return;
	/*
	if(dir == READ_DIR_H)
	{
		UV_Swap2(t0, t1);
		UV_Swap2(t2, t3);
		return;
	}
	*/
	if(dir == READ_DIR_V)
	{
		UV_Swap2(t0, t3);
		UV_Swap2(t1, t2);
		return;
	}
	/*
	if(dir == READ_DIR_HV)
	{
		UV_Swap2(t0, t1);
		UV_Swap2(t2, t3);
		return;
	}*/
}



int32_t _backface_cull_test(fix16_vec3_t* v0, fix16_vec3_t* v1, fix16_vec3_t* v2)
{
	render_t* render;
	render_transform_t* render_transform;
	
	const int32_vec2_t a = {
		v2->x - v0->x,
		v2->y - v0->y,
	};
	
	const int32_vec2_t b = {
		v1->x - v0->x,
		v1->y - v0->y,
	};
	
	const int32_t z = (a.x * b.y) - (a.y * b.x);
	
	return (z <= 0);
}



void render_mesh_transform(mesh_t* mesh)
{
	//printf("size of attribute: %ld \n", sizeof(attribute_t));
	
	POLYGON poly = {
		.zSort = 0,
		.flags = { .draw_mesh = false, .draw_paletted = false, .highSpeedShrink = false, .textureID = 0 },
		.v[0] = { { 0, 0 }, { 0, 0 }, { 255, 255, 255 } },
		.v[1] = { { 0, 0 }, { 255, 0 }, { 255, 255, 255 } }, 
		.v[2] = { { 0, 0 }, { 255, 255 }, { 255, 255, 255 } },
		.v[3] = { { 0, 0 }, { 0, 255 }, { 255, 255, 255 } }
	};
	
	FILE* file = NULL;
	
	if(mesh_OutputPolygonInfo)
	{
		file = fopen("DisplayListOutput.txt", "w");
	}
	
	for(uint16_t i = 0; i < mesh->polygons_count; i++)
	{
		attribute_t attr = mesh->attributes[i];
		//printf("Texture id %d for polygon: %d\n", attr.texture_slot, i);
		
		fix16_vec3_t transformed[4];
		
		poly.zSort = 0;
		
		poly.flags.draw_mesh = false;
		poly.flags.draw_paletted = false;
		poly.flags.highSpeedShrink = false;
		poly.flags.textureID = mesh->attributes[i].texture_slot;

		//poly.textureID = 0;
		
		texture_t* tex = &__state.tlist.textures[poly.flags.textureID];
		
		poly.v[1].uv.x = tex->size.x;
		poly.v[2].uv.x = tex->size.x;
		poly.v[2].uv.y = tex->size.y;
		poly.v[3].uv.y = tex->size.y;
		
		read_dir_t texDirection = attr.control;
		if(texDirection != READ_DIR_NORMAL) printf("polygon %d texture direction: %d\n", i, texDirection);
		assert(texDirection < READ_DIR_MAX);
		
		UV_Swap(&poly.v[0].uv, &poly.v[1].uv, &poly.v[2].uv, &poly.v[3].uv, texDirection);
		
		
		
		for(int j = 0; j < INDICES_PER_POLY; j++)
		{
			uint16_t index = (i * 4) + j;
			transformed[j] = mesh->points[mesh->Indices[index]];
			matrix_eval(&transformed[j]); //rewrite this to make it obvious what datatype we're working with
			
			int32_vec2_t newPos = { transformed[j].x, transformed[j].y }; 
			poly.v[j].pos = newPos;
		
			if(mesh->Colours)
			{
				uint16_t col = mesh->Colours[i];
				//poly.v[j].col.r = (((((col ^ 0x007C) >> 10) << 16) / 31) * 255) >> 16;
				//poly.v[j].col.g = ((((col ^ 0b111000000000011) >> 5) / 31) * 255) >> 16;
				//poly.v[j].col.b = ((((col ^ 0x1F) << 16) / 31) * 255) >> 16; 
			}
			
			poly.zSort = fix16_add(poly.zSort, transformed[j].z);
		}
		
		
		int skipPoly = 0;
		for(int j = 0; j < INDICES_PER_POLY; j++)
		{
			if(transformed[j].z < fix16_one || transformed[j].z > (2 << 16))
			{
				skipPoly = 1;
			}
		}
		
		if(skipPoly)
		{
			continue;
		}
		
		
		//if(_backface_cull_test(&transformed[0], &transformed[1], &transformed[2]))
		//{
		//	continue;
		//}
		
		poly.zSort = fix16_div(poly.zSort, fix16_one * 4);
		
		printf("%f\n", fix16_to_float(poly.zSort));
		if(poly.zSort < fix16_one)
		{
			continue;
		}
		
		if(QuadVisible(&poly, &clipRect))
		{
			continue;
		}
		
		DisplayList_AddPolygon(poly);
		
		if(mesh_OutputPolygonInfo)
		{

			
			fprintf(file, "Polygon TextureID: %d ZSort: %d(%f)\n", poly.flags.textureID, poly.zSort, fix16_to_float(poly.zSort));
			fprintf(file, "    X0: %d Y0: %d Z0: %d -- X1: %d Y1: %d Z1: %d\n", 
				transformed[0].x, transformed[0].y, transformed[0].z, 
				transformed[1].x, transformed[1].y, transformed[1].z);

			fprintf(file, "    X3: %d Y3: %d Z3: %d -- X2: %d Y2: %d Z2: %d\n\n", 
				transformed[3].x, transformed[3].y, transformed[3].z,
				transformed[2].x, transformed[2].y, transformed[2].z);
		}
	}
	
	if(mesh_OutputPolygonInfo)
	{
		printf("wrote mesh info to file\n");
		fclose(file);
	}
}


mesh_t* mesh_LoadFromFile(const char* filename, int32_t* count)
{
	FILE* fp = fopen(filename, "rb");
	if(fp == NULL)
	{
		printf("failed to open file %s\n", filename);
		return NULL;
	}
	
	fseek(fp, 0, SEEK_END);
	long siz = ftell(fp);
	//printf("size of file is %ld\n", siz);
	rewind(fp);
	uint8_t* buffer = (uint8_t*)malloc(siz + 1);
	//printf("preparing to read file\n");
	if(fread(buffer, 1, siz, fp) == 0)
	{
		printf("Failed to read file into memory\n");
	}
	
	fclose(fp);
	
	
	uint32_t magic = buffer[0];
	
	memcpy(&magic, buffer, sizeof(uint32_t));
	
	if(magic == MESH_MAGIC_CURRENT)
	{
		printf("read magic correctly\n");
	}
	else
	{
		printf("no magic: %d\n", magic);
		free(buffer);
		return NULL;
	}
	
	
	uint32_t meshCount = 0;
	memcpy(&meshCount, buffer + 4, 4);
	
	printf("number of meshes: %d\n", meshCount);
	
	*count = meshCount;
	
	mesh_t* meshPtr = (mesh_t*)malloc(sizeof(mesh_t) * meshCount); 
	

	for(uint32_t i = 0; i < meshCount; i++)
	{
		int32_t vertexOffset = 0, normalOffset = 0, colourOffset = 0, attributeOffset = 0, indexOffset = 0;
		
		memcpy(&meshPtr[i].polygons_count, buffer + (HEADER_BYTE_SIZE * i) + HEADER_OFFSET, sizeof(uint32_t));
		memcpy(&vertexOffset, buffer + (HEADER_BYTE_SIZE * i)+ HEADER_OFFSET + 4, sizeof(int32_t));
		memcpy(&normalOffset, buffer + (HEADER_BYTE_SIZE * i)+ HEADER_OFFSET + 8, sizeof(int32_t));
		memcpy(&colourOffset, buffer + (HEADER_BYTE_SIZE * i)+ HEADER_OFFSET + 12, sizeof(int32_t));
		memcpy(&attributeOffset, buffer + (HEADER_BYTE_SIZE * i) + HEADER_OFFSET + 16, sizeof(int32_t));
		memcpy(&indexOffset, buffer + (HEADER_BYTE_SIZE * i)+ HEADER_OFFSET + 20, sizeof(int32_t));
		
		
		
		meshPtr[i].points = (fix16_vec3_t*)(buffer + vertexOffset);
		meshPtr[i].normals = (fix16_vec3_t*)(buffer + normalOffset);
		if(colourOffset != -1)
		{
			meshPtr[i].Colours = (uint16_t*)(buffer + colourOffset);
		}
		else
		{
			meshPtr[i].Colours = NULL;
		}
		meshPtr[i].attributes = (attribute_t*)(buffer + attributeOffset);
		meshPtr[i].Indices = (uint16_t*)(buffer + indexOffset);
		
		printf("Poly Count: %d VertOffset %d NormalOffset %d ColourOffset %d AttributeOffset %d IndexOffset %d\n", 
			meshPtr[i].polygons_count, 
			vertexOffset, 
			normalOffset, 
			colourOffset, 
			attributeOffset,
			indexOffset);
		
	}
	
	
	//printf("got to end without crashing?\n");
	
	return meshPtr;
}

