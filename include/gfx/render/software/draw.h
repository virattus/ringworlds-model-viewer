#ifndef __RINGWORLDS_DRAW_H__
#define __RINGWORLDS_DRAW_H__


#include <gfx/primitive.h>
#include <gfx/texturemapper.h>
#include <gfx/vram.h>


typedef struct _drawstate
{
	VRAM* textureRam;
	int	use_texture;
	int draw_mesh;
	int draw_transparent;
	uint16_t texture;
	VRAM* frameBuffer;
	
} DrawState_t;


void DrawLine(DrawState_t* state, VERTEX* v0, VERTEX* v1);


void DrawSpan(DrawState_t* state, VERTEX* v0, VERTEX* v1);


void DrawPolyLine(DrawState_t* state, VERTEX* v0, VERTEX* v1, VERTEX* V2, VERTEX* V3);


void DrawPolygon(DrawState_t* state, VERTEX* v0, VERTEX* v1, VERTEX* v2, VERTEX* v3);


#endif
