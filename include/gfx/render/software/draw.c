#include <gfx/render/software/draw.h>

#include <core/colour.h>
#include <core/window.h>
#include <gfx/vdp1/vdp1.h>
#include <gamemath/vectormath.h>
#include <gfx/render.h>

#include <stddef.h>
#include <assert.h>


/*
 * it depends on color mode you use but the best way to convert color to "saturn" palette is to use these macros :

#define RGB( r, g, b ) (0x8000U|((b) << 10)|((g) << 5 )|(r))
#define RGB16_COLOR(r,g,b) ()(((b)<<10) + ((g)<<5) + (r) + 0x8000)
//Make 16 bit RGB Color
#define RGB32_COLOR(r,g,b) (Rgb32)(((b)<<16) + ((g)<<8) + (r) + 0x80000000)

from https://segaxtreme.net/threads/saturn-color-palette.23902/
might help out, I haven't read it all yet to fully understand what it is. 
Outside of that, from what I can gather, there are two different modes of coloring.
VDP1: 256 to 32,768
VDP2: 16 colors (4-bit) to 16,777,216 colors
(from https://segaretro.org/Sega_Saturn/Technical_specifications) (edited)


*/


int32_t RangeTest(int32_t value, int32_t min, int32_t max)
{
	if(value < min || value > max)
	{
		return 0;
	}
	
	return 1;
}



void DrawLine(DrawState_t* state, VERTEX* v0, VERTEX* v1)
{	
	int x, y;
	int dx, dy;
	int sx, sy;
	int err, e2;
	
	int xMin = min(v0->pos.x, v1->pos.x), yMin = min(v0->pos.y, v1->pos.y);
	int xMax = max(v0->pos.x, v1->pos.x), yMax = max(v0->pos.y, v1->pos.y);

	int numerator = 0;	
	int denominator = max(xMax - xMin, yMax - yMin) + 1;
	
	dx = v1->pos.x >= v0->pos.x ? v1->pos.x - v0->pos.x : v0->pos.x - v1->pos.x;
	dy = v1->pos.y >= v0->pos.y ? v0->pos.y - v1->pos.y : v1->pos.y - v0->pos.y;
	sx = v0->pos.x < v1->pos.x ? 1 : -1;
	sy = v0->pos.y < v1->pos.y ? 1 : -1;
	
	err = dx + dy;
	
	x = v0->pos.x;
	y = v0->pos.y;
	
	VRAM* fb = VDP1_GetFramebuffer(0);
	
	while(1)
	{
		int32_vec2_t pos = { x, y };		
		
		CVECTOR col = Interpolate_CVECTOR(v0->col, v1->col, fix16_div(fix16_from_int(numerator), fix16_from_int(denominator)));
		VRAM_WritePixel(fb, &pos, CVector_to_uint32(col));
		
		numerator++;

		
		if((x == v1->pos.x) && (y == v1->pos.y))
			break;
		
		e2 = 2 * err;
		
		if(e2 >= dy)
		{
			err += dy;
			x += sx;
		}
		if(e2 <= dx)
		{
			err += dx;
			y += sy;
		}
	}
}


void DrawSpan(DrawState_t* state, VERTEX* v0, VERTEX* v1)
{
	int distance = v1->pos.x - v0->pos.x;
	if(distance <= 0) distance++;

	for(int i = 0; i < distance; i += 1)
	{
		int32_vec2_t newPos = { v0->pos.x + i, v0->pos.y };
				
		if(!RangeTest(newPos.x, -1024, 1024) || !RangeTest(newPos.y, -1024, 1024))
			continue;
			
		if(state->draw_mesh)
		{
			//if((newPos.x % 2 == 0) && (newPos.y & 2 == 1))
				//continue;
		}
		
		CVECTOR newColour = Interpolate_CVECTOR(v0->col, v1->col, fix16_div(fix16_from_int(i), fix16_from_int(distance)));
		
			
		if(state->use_texture)
		{
			int32_vec2_t newUV = int32_vec2_interpolate(&v0->uv, &v1->uv, fix16_div(fix16_from_int(i), fix16_from_int(distance)));
			if(!RangeTest(newUV.x, 0, 512) || !RangeTest(newUV.y, 0, 512))
				continue;

			texture_t* tex = (texture_t*)(&__state.tlist.textures[state->texture]);
			uint32_t offset = tex->vram_offset + (newUV.y * tex->size.x) + newUV.x;
			uint32_t texColour = VRAM_ReadPixel(state->textureRam, offset);
		
			//temp until we learn how to blend
			//newColour = Add_CVECTOR(Scale_CVECTOR(newColour, 32767), Scale_CVECTOR(Uint32_to_CVector(texColour), 32767));
			
			newColour = Uint32_to_CVector(texColour);
		}
		
		VRAM_WritePixel(state->frameBuffer, &newPos, CVector_to_uint32(newColour));
	}
}


void DrawPolyLine(DrawState_t* state, VERTEX* v0, VERTEX* v1, VERTEX* v2, VERTEX* v3)
{
	DrawLine(state, v0, v1);
	DrawLine(state, v1, v2);
	DrawLine(state, v2, v3);
	DrawLine(state, v3, v0);
}


////DRAW POLYGON HELPER FUNCTIONS ////

typedef struct _VertSpan
{
	VERTEX min, max;
} VertSpan;


void BresenhamFindNodes(VERTEX* v0, VERTEX* v1, VertSpan* entries, int yBase, int entrySize)
{	
	int x, y;
	int dx, dy;
	int sx, sy;
	int err, e2;
	
	int xMin = min(v0->pos.x, v1->pos.x), yMin = min(v0->pos.y, v1->pos.y);
	int xMax = max(v0->pos.x, v1->pos.x), yMax = max(v0->pos.y, v1->pos.y);

	int numerator = 0;	
	int denominator = max(xMax - xMin, yMax - yMin) + 1;
	
	dx = v1->pos.x >= v0->pos.x ? v1->pos.x - v0->pos.x : v0->pos.x - v1->pos.x;
	dy = v1->pos.y >= v0->pos.y ? v0->pos.y - v1->pos.y : v1->pos.y - v0->pos.y;
	sx = v0->pos.x < v1->pos.x ? 1 : -1;
	sy = v0->pos.y < v1->pos.y ? 1 : -1;
	
	err = dx + dy;
	
	x = v0->pos.x;
	y = v0->pos.y;
	
	while(1)
	{
		int32_vec2_t pos = { x, y };
		int32_vec2_t uv = int32_vec2_interpolate(&v0->uv, &v1->uv, fix16_div(fix16_from_int(numerator), fix16_from_int(denominator)));
		CVECTOR col = Interpolate_CVECTOR(v0->col, v1->col, fix16_div(fix16_from_int(numerator), fix16_from_int(denominator)));
		
		VERTEX vert = { pos, uv, col };
		
		int entryID = y - yBase;
		
		if(entryID > entrySize - yBase + 1)
		{
			printf("Attempt to write beyond array? %d range %d : %d \n", entryID, yBase, entrySize);
		}
		
		if(entries[entryID].min.pos.x == 0xFFFFFFFF ||x < entries[entryID].min.pos.x)
		{
			entries[entryID].min = vert;
		}
		if(entries[entryID].max.pos.x == 0xFFFFFFFF || x > entries[entryID].max.pos.x)
		{
			entries[entryID].max = vert;
		}
		numerator++;

		
		if((x == v1->pos.x) && (y == v1->pos.y))
			break;
		
		e2 = 2 * err;
		
		if(e2 >= dy)
		{
			err += dy;
			x += sx;
		}
		if(e2 <= dx)
		{
			err += dx;
			y += sy;
		}
	}
}

/////// END POLYGON DRAW HELPER FUNCTIONS ///////////////


void DrawPolygon(DrawState_t* state, VERTEX* v0, VERTEX* v1, VERTEX* v2, VERTEX* v3)
{
	int vertexCount = 4;
	
	int xMin = v0->pos.x, xMax = v0->pos.x;
	int yMin = v0->pos.y, yMax = v0->pos.y;

	xMin = min(xMin, v1->pos.x);
	yMin = min(yMin, v1->pos.y);
	xMax = max(xMax, v1->pos.x);
	yMax = max(yMax, v1->pos.y);
	
	xMin = min(xMin, v2->pos.x);
	yMin = min(yMin, v2->pos.y);
	xMax = max(xMax, v2->pos.x);
	yMax = max(yMax, v2->pos.y);
	
	xMin = min(xMin, v3->pos.x);
	yMin = min(yMin, v3->pos.y);
	xMax = max(xMax, v3->pos.x);
	yMax = max(yMax, v3->pos.y);
	
	//printf("xMin: %d xMax %d yMin: %d yMax: %d\n", xMin, xMax, yMin, yMax);
	
	int range = yMax - yMin + 1;
	//printf("vertical range: %d\n", range);
	
	
	VertSpan* entries = malloc(sizeof(VertSpan) * range);
	memset(entries, 0xFFFFFFFF, sizeof(VertSpan) * range);
	
	
	//printf("Line 0 and 1\n");
	BresenhamFindNodes(v0, v1, entries, yMin, yMax);
	
	//printf("Line 1 and 2\n");
	BresenhamFindNodes(v1, v2, entries, yMin, yMax);
	
	//printf("Line 2 and 3\n");
	BresenhamFindNodes(v2, v3, entries, yMin, yMax);
	
	//printf("Line 3 and 0\n");
	BresenhamFindNodes(v3, v0, entries, yMin, yMax);
	

	for(int i = 0; i < range - 1; i++)
	{
		//printf("Entry %d min x: %d y: %d, max x: %d y: %d\n", i, entries[i].min.pos.x, entries[i].min.pos.y, entries[i].max.pos.x, entries[i].max.pos.y);
		
		int32_t dist = entries[i].max.pos.x - entries[i].min.pos.x;
		if(dist < 0)
		{
			printf("ERROR: negative distance for span: (%d) - (%d)\n", entries[i].max.pos.x, entries[i].min.pos.x);
		}
		
		//DrawSpan(entries[i].min.pos, dist, entries[i].min.col, entries[i].max.col); 
		DrawSpan(state, &entries[i].min, &entries[i].max);
	}
	
	
	free(entries);
}
