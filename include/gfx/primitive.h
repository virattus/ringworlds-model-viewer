#ifndef __RINGWORLDS_PRIMITIVE_H__
#define __RINGWORLDS_PRIMITIVE_H__



#include <gamemath/vector.h>
#include <core/colour.h>

#include <libfixmath/fix16.h>

#include <stdbool.h>


typedef struct _RECT
{
	int32_vec2_t min;
	int32_vec2_t max;
} RECT;


typedef struct _VERTEX
{
	int32_vec2_t pos;
	int32_vec2_t uv;
	CVECTOR col;
} VERTEX;


typedef struct _DRAWPOLY_FLAGS
{
	bool draw_mesh;
	bool draw_paletted;
	bool highSpeedShrink;
	uint16_t textureID;
	
} POLYGON_FLAGS;


typedef struct _POLYGON
{
	fix16_t zSort;
	POLYGON_FLAGS flags;
	VERTEX v[4];
} POLYGON;


#endif
