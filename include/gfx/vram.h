#ifndef __RINGWORLDS_VRAM_H__
#define __RINGWORLDS_VRAM_H__



#include <gamemath/vector.h>
#include <core/colour.h>

#include <SDL2/SDL.h>
#include <stdint.h>



typedef struct _VRAM{
	SDL_Surface* RAMSurface;
	int32_vec2_t Dimensions;
} VRAM;


void VRAM_Init(VRAM* ram, const int32_vec2_t* dimensions, uint32_t colour);

void VRAM_Fill(VRAM* ram, uint32_t colour);

void VRAM_WriteTexture(VRAM* ram, uint32_t offset, int16_vec2_t* dimensions, const char* filename);

void VRAM_WritePixel(VRAM* ram, const int32_vec2_t* pos, uint32_t colour);

uint32_t VRAM_ReadPixel(const VRAM* ram, uint32_t offset);
uint32_t VRAM_ReadPixel_UV(const VRAM* ram, const int32_vec2_t* UV);
CVECTOR VRAM_ReadPixel_CVEC(const VRAM* ram, uint32_t offset);



#endif
