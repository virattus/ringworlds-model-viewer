#include <gfx/vram.h>

#include <gfx/render.h>
#include <gfx/texturemapper.h>

#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>


#define SDL_A_MASK 0x000000FF
#define SDL_R_MASK 0xFF000000
#define SDL_G_MASK 0x00FF0000
#define SDL_B_MASK 0x0000FF00


void VRAM_Init(VRAM* ram, const int32_vec2_t* dimensions, uint32_t colour)
{
	ram->Dimensions = *dimensions;
	ram->RAMSurface = SDL_CreateRGBSurface(0, dimensions->x, dimensions->y, 32, SDL_R_MASK, SDL_G_MASK, SDL_B_MASK, SDL_A_MASK);
	VRAM_Fill(ram, colour);
}


void VRAM_Fill(VRAM* ram, uint32_t colour)
{
	SDL_FillRect(ram->RAMSurface, NULL, colour);
}


void VRAM_WriteTexture(VRAM* ram, uint32_t offset, int16_vec2_t* dimensions, const char* filename)
{
	SDL_Surface* newtex = IMG_Load(filename);
	if(newtex == NULL)
	{
		printf("Failed to load texture %s with error: %s\n", filename, SDL_GetError());
		return;
	}
	
	SDL_Surface* oldsurface = newtex;
	
	newtex = SDL_ConvertSurfaceFormat(newtex, SDL_PIXELFORMAT_RGBA8888, 0);
	
	dimensions->x = newtex->w;
	dimensions->y = newtex->h;
	
	if(dimensions->x > 255 || dimensions->y > 255)
	{
		printf("Attempt to load texture with size greater than 255: %s\n", filename);
		SDL_FreeSurface(oldsurface);
		SDL_FreeSurface(newtex);
		return;
	}
	
	if((offset * 4) + (dimensions->x * dimensions->y * 4) > (ram->Dimensions.x * ram->Dimensions.y * 4))
	{
		printf("Error: attempt to write past max VRAM: %s\n", filename);
		SDL_FreeSurface(oldsurface);
		SDL_FreeSurface(newtex);
		return;
	}

	memcpy(ram->RAMSurface->pixels + (offset * 4), newtex->pixels, dimensions->x * dimensions->y * 4); 
	
	SDL_FreeSurface(oldsurface);
	SDL_FreeSurface(newtex);
}


void VRAM_WritePixel(VRAM* ram, const int32_vec2_t* pos, uint32_t colour)
{
	
	if((pos->x < 0 || pos->x >= ram->Dimensions.x) ||
		(pos->y < 0 || pos->y >= ram->Dimensions.y))
	{
		return;
	}
	
	uint32_t* pixels = (uint32_t*)ram->RAMSurface->pixels;
	pixels[(pos->y * ram->Dimensions.x) + pos->x] = colour;
}


uint32_t VRAM_ReadPixel(const VRAM* ram, uint32_t offset)
{
	uint32_t* pixels = (uint32_t*)ram->RAMSurface->pixels;
	return pixels[offset];
}


uint32_t VRAM_ReadPixel_UV(const VRAM* ram, const int32_vec2_t* UV)
{
	if((UV->x < 0 || UV->x >= ram->Dimensions.x) ||
		(UV->y < 0 || UV->y >= ram->Dimensions.y))
	{
		printf("VRAM_ReadPixel: Reading outside of ram: %d %d\n", UV->x, UV->y);
		return 0;
	}
	
	return VRAM_ReadPixel(ram, (UV->y * ram->Dimensions.x + UV->x));
}
